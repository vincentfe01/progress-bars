# Front End Assignment - Progress Bars

Primary task: Using vanilla JavaScript or any JavaScript library of your choosing (no jQuery), implement the following (you can make it look however you like)

## Features

<dl>
  <dt>Must read data from the endpoint</dt>
  <dt>One set of controls that can control each bar on the fly</dt>
  <dt>Can't go under 0</dt>
  <dt>Can go over limit (defined in API), but limit the bar itself and change its colour</dt>
  <dt>Display usage amount, centered</dt>
  <dt>Write tests for your code (hint: TDD strongly preferred)</dt>
  <dt>Implement a responsive solution: testing it on mobile, tablet, etc. Getting it working nicely.</dt>
  <dt>Animate the bar change, make sure it works well when you tap buttons quickly.</dt>
  <dt>Version control (git)</dt>
  <dt>Setting it up as a project</dt>
  <dt>Setting up some automated tools</dt>
  <dt>Linting, code quality, etc</dt>
  <dt>JavaScript/CSS minification, packaging, etc</dt>
  <dt>Using a CSS preprocessor like SASS/SCSS</dt>
  <dt>Styling it to a production quality level</dt>
</dl>

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

import React from 'react';
import Bars from 'containers/BarsLoader';
import ErrorBoundary from 'components/ErrorBoundary';

export default () => (
  <ErrorBoundary>
    <div className="app-body">
      <Bars />
    </div>
  </ErrorBoundary>
);

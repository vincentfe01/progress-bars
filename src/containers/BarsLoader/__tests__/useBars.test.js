import { renderHook, act } from '@testing-library/react-hooks';
import service from 'utils/service';
import MockAdapter from 'axios-mock-adapter';
import { API_BARS } from 'utils/constants';
import produce from 'immer';
import { useReducer } from 'react';
import { CHANGE_PROGRESS, CHANGE_BAR, UPDATE_DATA } from '../constants';
import { useBars, barsReducer } from '../hooks';

const mockData = {
  buttons: [10, 20, -6, -44],
  bars: [71, 10, 68],
  limit: 140,
  active: 0,
};
const updatedState = {
  buttons: [10, 20, -6, -44],
  bars: {
    0: 71,
    1: 10,
    2: 68,
  },
  limit: 140,
  active: 0,
};

describe('The useBars hook', () => {
  it('should return initial state when being called', async () => {
    const { result } = renderHook(() => useBars());
    expect(result.current.barState).toEqual(null);
    expect(result.current.loading).toBeTruthy();
  });

  it('should performs GET request and dispatch the response to reducer, then return the new barState value', async () => {
    const mock = new MockAdapter(service);
    mock.onGet(API_BARS).reply(200, mockData);
    const { result, waitForNextUpdate } = renderHook(() => useBars());
    await waitForNextUpdate();
    expect(result.current.barState).toEqual(updatedState);
    expect(result.current.loading).toBeFalsy();
  });

  it('should return initial barState on network error', async () => {
    const mock = new MockAdapter(service);
    mock.onGet(API_BARS).networkError();
    const { result, waitForNextUpdate } = renderHook(() => useBars());
    await waitForNextUpdate();
    expect(result.current.barState).toEqual(null);
    expect(result.current.loading).toBeFalsy();
  });
});

describe('The reducer of useBars', () => {
  const initialState = null;

  it('should update data', () => {
    const newState = barsReducer(initialState, {
      type: UPDATE_DATA,
      data: mockData,
    });
    expect(newState).toEqual(updatedState);
  });

  it('should change bar', () => {
    const newState = barsReducer(updatedState, {
      type: CHANGE_BAR,
      index: 1,
    });
    expect(newState).toEqual({
      ...updatedState,
      active: 1,
    });
  });

  it('should change progress', () => {
    const newState = barsReducer(updatedState, {
      type: CHANGE_PROGRESS,
      val: 20,
    });
    const expectedState = produce(updatedState, draft => {
      // eslint-disable-next-line no-param-reassign
      draft.bars[updatedState.active] = 91;
    });
    expect(newState).toEqual(expectedState);
  });

  it('should throw when dispatch wrong type', () => {
    expect.assertions(1);
    const { result } = renderHook(() => useReducer(barsReducer, initialState));
    const [, dispatch] = result.current;
    act(() => {
      dispatch({ type: 'WRONG_TYPE' });
    });
    expect(result.error).toEqual(Error('Unknown action type'));
  });
});

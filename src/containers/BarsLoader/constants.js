export const CHANGE_PROGRESS = 'progress-bars-demo/BarsLoader/CHANGE_PROGRESS';
export const CHANGE_BAR = 'progress-bars-demo/BarsLoader/CHANGE_BAR';
export const UPDATE_DATA = 'progress-bars-demo/BarsLoader/UPDATE_DATA';
